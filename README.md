# UC

This service is developed to count users visited home page of the website

**ATTENTION:**
This app uses cookies to identify users.
It is RECOMENDED to use Firefox for proper visits counting.

**How to run?**

1. Clone or download this repository.
2. Open uc folder in your terminal. Check if 8181 port is clear. You can eddit this line `defaultPort = ":8181` in **_server.go_** to change port.
3. Run command `go run server.go`.

   3.1. (optional) Deploy app with **_ngrok_** using `./ngrok http 8181` command.
4. Open http://localhost:8181/ in several browsers or use incognito mode to simulate different users. (OR use other devices if you deployed app with ngrok).

**How to stop?**

Press **_Ctrl+C_** to stop running server and clear the port. If program was interupted with the wrong way run `fuser -k 8181/tcp` command to kill listener and clear port.
