package main

import (
	"fmt"
	"log"
	"net/http"
	"sort"
	"strconv"
	"time"
)

const (
	defaultPort = ":8181"
)

var (
	usersIDList  []int
	visitCounter int = 0
)

func cookieHandler(w http.ResponseWriter, r *http.Request) {
	id, err := readCookie(w, r)
	visitCounter++
	if err != nil { // no ID in cookies
		newUserCreator(w)
		displayInfo(w)
	} else { //  ID is in cookies
		intID, convErr := strconv.Atoi(id) // parse cookie value to int
		if convErr != nil {
			log.Println("Strconv error atoi: " + convErr.Error())
		}
		if intID <= 0 || convErr != nil {
			log.Printf("Unexpected ID: %s. Creating new user...\n", id)
			newUserCreator(w)
			displayInfo(w)
		} else {
			if !binarySearch(usersIDList, intID) { // no such ID in usersIDList
				log.Printf("ID not found: %d. Inserting in memory...\n", intID)
				usersIDList = insertSorted(usersIDList, intID) // add ID from cookies
				displayInfo(w)
			} else { // found ID in usersIGList
				log.Printf("ID found: %d\n", intID)
				displayInfo(w)
			}
		}
	}
}

func readCookie(w http.ResponseWriter, r *http.Request) (id string, err error) {
	c, err := r.Cookie("ID")
	if err != nil {
		log.Println("Reading cookie error: " + err.Error())
		return "", err
	}
	return c.Value, nil
}

func createCookie(w http.ResponseWriter, ident string) {
	expiry := time.Now().AddDate(0, 0, 1) // cookie alive for 24 hours
	c := &http.Cookie{
		Name:    "ID",
		Expires: expiry,
		Value:   ident,
	}
	http.SetCookie(w, c)
	log.Println("Created new user ID: " + c.Value)
	w.Write([]byte("New user visited this page!\n"))
}

func newUserCreator(w http.ResponseWriter) {
	newID := idGenerator(usersIDList)
	newIDstring := strconv.Itoa(newID)
	usersIDList = append(usersIDList, newID)
	createCookie(w, newIDstring)
}

func displayInfo(w http.ResponseWriter) {
	w.Write([]byte("Users visited: " + strconv.Itoa(len(usersIDList))))
	w.Write([]byte("\nThis page has been visited " + (strconv.Itoa(visitCounter)) + " times"))
}

func idGenerator(IDList []int) (newID int) {
	if len(IDList) != 0 {
		newID = IDList[len(IDList)-1] + 1
		log.Printf("Generated new ID: %d.\n", newID)
	} else {
		newID = 1
		log.Printf("Generated new ID: %d.\n", newID)
	}
	return
}

func binarySearch(array []int, search int) (result bool) {
	mid := len(array) / 2
	switch {
	case len(array) == 0:
		result = false // not found
	case array[mid] > search:
		result = binarySearch(array[:mid], search)
	case array[mid] < search:
		result = binarySearch(array[mid+1:], search)
	default: // a[mid] == search
		result = true // found
	}
	return
}

func insertSorted(array []int, a int) []int {
	i := sort.Search(len(array), func(i int) bool { return array[i] > a })
	array = append(array, 0)
	copy(array[i+1:], array[i:])
	array[i] = a
	log.Printf("Inserted ID: %d.\n", a)
	return array
}

func main() {
	http.HandleFunc("/", cookieHandler)
	fmt.Println("Server is listening...")
	err := http.ListenAndServe(defaultPort, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
