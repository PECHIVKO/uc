package main

import (
	"reflect"
	"testing"
)

func Test_idGenerator(t *testing.T) {
	type args struct {
		IDList []int
	}
	tests := []struct {
		name      string
		args      args
		wantNewID int
	}{
		{
			name:      "test_empty_slice",
			args:      args{},
			wantNewID: 1,
		},
		{
			name:      "test_filled_slice",
			args:      args{[]int{1, 2, 3}},
			wantNewID: 4,
		},
		{
			name:      "test_negative_slice",
			args:      args{[]int{-4, -3, -2}},
			wantNewID: -1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotNewID := idGenerator(tt.args.IDList); gotNewID != tt.wantNewID {
				t.Errorf("idGenerator() = %v, want %v", gotNewID, tt.wantNewID)
			}
		})
	}
}

func Test_binarySearch(t *testing.T) {
	type args struct {
		array  []int
		search int
	}
	tests := []struct {
		name       string
		args       args
		wantResult bool
	}{
		{
			name: "test_found",
			args: args{
				array:  []int{7, 8, 9, 10},
				search: 9,
			},
			wantResult: true,
		},
		{
			name: "test_not_found",
			args: args{
				array:  []int{1, 2, 3, 4},
				search: 9,
			},
			wantResult: false,
		},
		{
			name: "test_empty",
			args: args{
				array:  []int{},
				search: 9,
			},
			wantResult: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := binarySearch(tt.args.array, tt.args.search); gotResult != tt.wantResult {
				t.Errorf("binarySearch() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func Test_insertSorted(t *testing.T) {
	type args struct {
		array []int
		a     int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "test_insert_last",
			args: args{
				array: []int{1, 2, 3, 4},
				a:     5,
			},
			want: []int{1, 2, 3, 4, 5},
		},
		{
			name: "test_insert_first",
			args: args{
				array: []int{2, 3, 4},
				a:     1,
			},
			want: []int{1, 2, 3, 4},
		},
		{
			name: "test_insert_mid",
			args: args{
				array: []int{1, 2, 4, 5},
				a:     3,
			},
			want: []int{1, 2, 3, 4, 5},
		},
		{
			name: "test_insert_negative",
			args: args{
				array: []int{1, 2, 4, 5},
				a:     -3,
			},
			want: []int{-3, 1, 2, 4, 5},
		},
		{
			name: "test_insert_big",
			args: args{
				array: []int{1, 2, 4, 5},
				a:     999,
			},
			want: []int{1, 2, 4, 5, 999},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := insertSorted(tt.args.array, tt.args.a); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("insertSorted() = %v, want %v", got, tt.want)
			}
		})
	}
}
